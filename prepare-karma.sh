#!/bin/sh

sed -i -e "/browsers: \['Chrome'\],/r /ng-cli-conf/headless-karma-conf.json.part" src/karma.conf.js
sed -i -e "/browsers: \['Chrome'\],/d" src/karma.conf.js
